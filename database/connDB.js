const mysql = require('mysql') // เรียกใช้ mysql

const connDB = mysql.createConnection({   // config ค่าการเชื่อมต่อฐานข้อมูล
    host     : 'localhost', 
    user     : 'root',
    port : 3306,
    password : '0000',
    database : 'chic-chat'
    })
    
    connDB.connect(function(err) {
        if (err) throw err;
        console.log('Connected!');
       });

module.exports = connDB
const errorMessage = {
    'err_database': { 'code': '0001', 'message': 'There Was a Problem in Database.' }, 
    'err_username_already': { 'code': '0002', 'message': 'Username is already.' },
    'err_email_already': { 'code': '0003', 'message': 'Email is already.' },
    'err_citizen_already': { 'code': '0004', 'message': 'Citizen is Already.' },
    'err_wrong_password' : { 'code': '0005', 'message': 'Wrong Password.'},
    'err_wrong_oldpass' : { 'code': '0007', 'message': 'Please enter the old password again.'},
    'err_wrong_Username' : { 'code': '0006', 'message': 'Username does not exist'},
    'err_Invalid_token' : { 'code': '0007', 'message': 'Invalid token'}
}

module.exports = errorMessage
const express = require('express')
const router = express.Router()
const connDB = require('../database/connDB')
const CreateToken = require('./controller/token')
const errorMessage = require('./const/errorMessage')

router.put('/editprofile/:idUser', CreateToken.validateToken(), CreateToken.updateNewToken(), (req, res) => {
  let username = req.body.username
  console.log
  let sqluser = "SELECT * FROM users WHERE username = '" + username + "' AND idUser NOT IN ('" + req.params.idUser + "')"
  connDB.query(sqluser, (error_checkUser, results_username, fields) => {
    if (error_checkUser) {
      console.log('error_checkUser', error_checkUser)
      res.send(
        JSON.stringify(errorMessage.err_database)
      )
    }
    if (results_username[0] === undefined) {
      let email = req.body.email
      let sqlemail = "SELECT * FROM users WHERE email = '" + email + "' AND idUser NOT IN ('" + req.params.idUser + "')"
      connDB.query(sqlemail, (error_checkEmail, results_email, fields_email) => {
        if (error_checkEmail) {
          console.log('error_checkEmail', error_checkEmail)
          res.send(
            JSON.stringify(errorMessage.err_database)
          )
        }
        if (results_email[0] === undefined) {
          let citizen = req.body.citizen
          let sqlcitizen = "SELECT * FROM users WHERE citizen = '" + citizen + "' AND idUser NOT IN ('" + req.params.idUser + "')"
          connDB.query(sqlcitizen, (error_citizen, results_citizen, fields) => {
            if (error_citizen) {
              console.log('error_citizen', error_citizen)
              res.send(
                JSON.stringify(errorMessage.err_database)
              )
            }
            if (results_citizen[0] === undefined) {
              let firstname = req.body.firstname
              let lastname = req.body.lastname
              let username = req.body.username
              let email = req.body.email
              let mobile = req.body.mobile
              let citizen = req.body.citizen
              let about = req.body.about
              let sqlupdateUser = "UPDATE users SET firstname = '" + firstname + "', lastname = '" + lastname + "', username = '" + username + "' , email = '" + email + "', mobile = '" + mobile + "', citizen = '" + citizen + "', about = '" + about + "' WHERE idUser = " + req.params.idUser
              connDB.query(sqlupdateUser, (error_updateUsers, results_updateUser, fields) => {
                if (error_updateUsers) {
                  console.log('error_updateUsers', error_updateUsers)
                  res.send(
                    JSON.stringify(errorMessage.err_database)
                  )
                }
                if (results_updateUser[0] === undefined) {
                  let firstname_friend = req.body.firstname
                  let lastname_friend = req.body.lastname
                  let username_friend = req.body.username
                  let email_friend = req.body.email
                  let mobile_friend = req.body.mobile
                  let aboutFriend = req.body.about
                  let sqlupdateFriend = "UPDATE friends SET firstname_friend = '" + firstname_friend + "', lastname_friend = '" + lastname_friend + "', username_friend = '" + username_friend + "' , email_friend = '" + email_friend + "', mobile_friend = '" + mobile_friend + "', aboutFriend = '" + aboutFriend + "' WHERE idFriend = " + req.params.idUser
                  connDB.query(sqlupdateFriend, (error_updateFriends, results) => {
                    if (error_updateFriends) {
                      console.log('error_updateFriends', error_updateFriends)
                      res.send(
                        JSON.stringify(errorMessage.err_database)
                      )
                    } else {
                      let username = req.body.username
                      let sqluserDetail = "SELECT * FROM users WHERE username = ?"
                      connDB.query(sqluserDetail, [username], (error_select_detailUser, results) => {
                        if (error_select_detailUser) {
                          console.log('error_select_detailUser', error_select_detailUser)
                          res.send(
                            JSON.stringify(errorMessage.err_database)
                          )
                        } else {
                          res.send({
                            code: '0000',
                            message: 'Success',
                            results: JSON.stringify(results),
                            token: req.token
                          })
                        }
                      })
                    }
                  })
                }
              })
            } else {
              res.send(
                JSON.stringify(errorMessage.err_citizen_already)
              )
            }
          })
        } else {
          res.send(
            JSON.stringify(errorMessage.err_email_already)
          )
        }
      })
    } else {
      res.send(
        JSON.stringify(errorMessage.err_username_already)
      )
    }
  })
})

module.exports = router
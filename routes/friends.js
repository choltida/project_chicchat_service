const express = require('express')
const router = express.Router()
const connDB = require('../database/connDB')
const CreateToken = require('./controller/token')
const errorMessage = require('./const/errorMessage')

//ค้นหาเพื่อน
router.post('/search', CreateToken.validateToken(), CreateToken.updateNewToken(), (req, res) => {
    let searchUser = req.body.search
    let sqlUser = "SELECT * FROM friends WHERE username_friend LIKE " + "'" + [searchUser] + "%'"
    connDB.query(sqlUser, (error_searchusername, results, fields) => {
        if (error_searchusername) {
            console.log('error_searchusername', error_searchusername)
            res.send(
                JSON.stringify(errorMessage.err_database)
            )
        }
        if (results[0] === undefined) {
            let searchEmail = req.body.search
            let sqlEmail = "SELECT * FROM friends WHERE email_friend = ? "
            connDB.query(sqlEmail, [searchEmail], (error_searchEmail, results, fields) => {
                if (error_searchEmail) {
                    console.log('error_searchEmail', error_searchEmail)
                    res.send(
                        JSON.stringify(errorMessage.err_database)
                    )
                }
                if (results[0] === undefined) {
                    let searchMobile = req.body.search
                    let sqlMobile = "SELECT * FROM friends WHERE mobile_friend = ? "
                    connDB.query(sqlMobile, [searchMobile], (error_searchMobile, results, fields) => {
                        if (error_searchMobile) {
                            console.log('error_searchMobile', error_searchMobile)
                            res.send(
                                JSON.stringify(errorMessage.err_database)
                            )
                        }
                        if (results[0] === undefined) {
                            res.send({
                                code: '201',
                                results: ''
                            })
                        } else {
                            res.send({
                                code: '0000',
                                message: 'Success',
                                results: JSON.stringify(results),
                                token: req.token
                            })
                            console.log('search by.mobile')
                        }
                    })
                } else {
                    res.send({
                        code: '0000',
                        message: 'Success',
                        results: JSON.stringify(results),
                        token: req.token
                    })
                    console.log('search by.email')
                }
            })
        } else {
            res.send({
                code: '0000',
                message: 'Success',
                results: JSON.stringify(results),
                token: req.token
            })
            console.log('search by.username')
        }
    })
})

//เรียกดูเพื่อนที่เป็นเพื่อน
router.get('/showfriend', CreateToken.validateToken(), CreateToken.updateNewToken(), (req, res) => {   // Router เวลาเรียกใช้งาน
    let sql = 'SELECT * FROM friends WHERE status=1'  // คำสั่ง sql
    connDB.query(sql, (err_showfriends, results, fields) => { // สั่ง Query คำสั่ง sql
        if (err_showfriends) { // ดัก error
            console.log('err_selectfriends', err_selectfriends)
            res.send(
                JSON.stringify(errorMessage.err_database)
            )
        } else {
            res.send({
                code: '0000',
                message: 'Success',
                friend_list: JSON.stringify(results),
                token: req.token
            })
        }
    })
})

//เรียกดูโปรไฟล์เพื่อน
router.get('/friend/:idFriend', CreateToken.validateToken(), CreateToken.updateNewToken(), (req, res) => {
    let sql = "SELECT * FROM friends WHERE idFriend = " + req.params.idFriend
    connDB.query(sql, (err_showdetailfriend, results, fields) => {
        if (err_showdetailfriend) {
            console.log('err_showdetailfriend', err_showdetailfriend)
            res.send(
                JSON.stringify(errorMessage.err_database)
            )
        } else {
            res.send({
                code: '0000',
                results: JSON.stringify(results),
                token: req.token
            })
        }
    })
})

//เพิ่มเป็นเพื่อน
router.put('/addfriend/:idFriend', CreateToken.validateToken(), CreateToken.updateNewToken(), (req, res) => {
    let sql = "UPDATE friends SET status='1' WHERE idFriend=" + req.params.idFriend
    connDB.query(sql, (err_addfriends, results, fields) => {
        if (err_addfriends) {
            console.log('err_addfriends', err_addfriends)
            res.send(
                JSON.stringify(errorMessage.err_database)
            )
        } else {
            res.send({
                code: '0000',
                message: 'Add friend success',
                results: JSON.stringify(results),
                token: req.token
            })
        }
    })
})

//เลิกเป็นเพื่อน
router.put('/unfriend/:idFriend', CreateToken.validateToken(), CreateToken.updateNewToken(), (req, res) => {
    let sql = "UPDATE friends SET status='0' WHERE idFriend=" + req.params.idFriend
    connDB.query(sql, (err_delfriend, results, fields) => {
        if (err_delfriend){
            console.log('err_delfriend', err_delfriend)
            res.send(
                JSON.stringify(errorMessage.err_database)
            )
        } else {
            res.send({
                code: '0000',
                message: 'Delete friend success',
                results: JSON.stringify(results),
                token: req.token
            })
        }
    })
})

module.exports = router
const express = require('express')
const router = express.Router()
const connDB = require('../database/connDB')
const passwordHash = require('../node_modules/password-hash')
const updateToken = require('./controller/token')
const jwt = require("jwt-simple")
const CreateToken = require('./controller/token')
const errorMessage = require('./const/errorMessage')

const password = require('password-hash')


//login
router.post('/login', (req, res) => {
  const username = req.body.username
  const password = req.body.password
  const sqluser = "SELECT * FROM users WHERE username = ? "
  connDB.query(sqluser, [username], (error_login, results, fields) => {
    if (error_login) {
      res.send(
        JSON.stringify(errorMessage.err_database)
      )
      console.log('error_login', error_login)
    }
    if (results[0] !== undefined) {
      const passwordDB = passwordHash.verify(password, results[0].password)
      if (passwordDB === true) {
        const payload = {
          username: req.body.username,
          id: results[0].idUser,
          iat: new Date().getTime()//มาจากคำว่า issued at time (สร้างเมื่อ)
        }
        const SECRET = "MY_SECRET_KEY"
        const token = jwt.encode(payload, SECRET)
        updateToken.updateToken(token)
        res.send({
          code: '0000',
          results: JSON.stringify(results),
          token: token
        })
      } else {
        res.send(
          JSON.stringify(errorMessage.err_wrong_password)
        )
      }
    } else {
      res.send(
        JSON.stringify(errorMessage.err_wrong_Username)
      )
    }
  })
})

//Change password
router.post('/changpassword', (req, res) => {
  const idUser = req.body.idUser
  const oldpass = req.body.oldPassword
  const newpass = password.generate(req.body.newPassword)
  const sqlidUser = "SELECT * FROM users WHERE idUser = ?"
  connDB.query(sqlidUser, [idUser], (error_checkidUser, results, fields) => {
    if (error_checkidUser) {
      console.log('error_checkidUser', error_checkidUser)
      res.send(
        JSON.stringify(errorMessage.err_database)
      )
    }
    if (results[0] !== undefined) {
      const passwordDB = passwordHash.verify(oldpass, results[0].password)
      if (passwordDB === true) {
        const sql = "UPDATE users SET password= '" + newpass + "' WHERE idUser = ?"
        connDB.query(sql, [idUser], (error_checkoldPass, results, fields) => {
          if (error_checkoldPass) {
            console.log('error_checkoldPass', error_checkoldPass)
            res.send(
              JSON.stringify(errorMessage.err_database)
            )
          } else {
            res.send({
              code: '0000',
              message: 'Change password success. Please log in again.',
              results: JSON.stringify(results)
            })
          }
        })
      } else {
        console.log('wrong password.')
        res.send(
          JSON.stringify(errorMessage.err_wrong_oldpass)
        )
      }
    } else {
      console.log('wrong idUser')
      res.send(
        JSON.stringify(errorMessage.err_wrong_Username)
      )
    }
  })
})

  module.exports = router
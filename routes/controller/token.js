const connDB = require('../../database/connDB')
const jwt = require("jwt-simple")
const SECRET = "MY_SECRET_KEY"
const errorMessage = require('../const/errorMessage')

exports.updateNewToken = function () {
    return function (req, res, next) {
        var token = req.headers.token
        const decode = jwt.decode(token, SECRET)
        id = decode.id
        username = decode.username

        /////// create new token

        const payload = {
            username: username,
            id: id,
            iat: new Date().getTime()//มาจากคำว่า issued at time (สร้างเมื่อ)
        }
        var newtoken = jwt.encode(payload, SECRET)

        /////////////////////

        var sqltoken = "SELECT * FROM token WHERE idUser = ? "
        connDB.query(sqltoken, [id], (error_selecttoken, resultstoken, fields) => {
            if (error_selecttoken) {
                console.log('error_selecttoken', error_selecttoken)
                res.send(
                    JSON.stringify(errorMessage.err_database)
                )
            }
            if (resultstoken[0] !== undefined) {
                var updatetoken = "UPDATE token SET token = '" + newtoken + "' WHERE idUser = ?"
                connDB.query(updatetoken, [id], (error_updatetoken, results, fields) => {
                    // console.log('newtoken', newtoken)
                    if (!error_updatetoken) {
                        req.token = newtoken
                        // console.log('set req token', req.token)
                    }
                    next()
                })
            } else {
                var inserttoken = "INSERT INTO token (idUser, token) VALUES ('" + [id] + "', '" + newtoken + "')"
                connDB.query(inserttoken, (error_inserttoken, resultstoken, fields) => {
                    console.log('Insert token')
                    if (!error_inserttoken) {
                        req.token = newtoken
                    }
                    next()
                })
            }

        })
    }

}

exports.validateToken = function () {
    return function (req, res, next) {
        var token = req.headers.token
        var sqltoken = "SELECT * FROM token WHERE token = ? "
        // console.log('validatetoken =', token)
        connDB.query(sqltoken, [token], (error_validateToken, resultstoken, fields) => {
            if (error_validateToken) {
                console.log('error_validateToken', error_validateToken)
                res.send(
                    JSON.stringify(errorMessage.err_database)
                )
            }
            if (resultstoken.length == 0) {
                res.send(
                    JSON.stringify(errorMessage.err_Invalid_token)
                )
            }
            else {
                console.log('find token success')
                next()
            }
        })
    }
}

exports.updateToken = (token) => {
    const decode = jwt.decode(token, SECRET)
    id = decode.id
    username = decode.username
    var sqltoken = "SELECT * FROM token WHERE idUser = ? "
    connDB.query(sqltoken, [id], (error_updatetoken, resultstoken, fields) => {
        if (error_updatetoken) {
            console.log('error_updatetoken', error_updatetoken)
            res.send(
                JSON.stringify(errorMessage.error_updatetoken)
            )
        }
        if (resultstoken[0] === undefined) {
            var inserttoken = "INSERT INTO token (idUser, token) VALUES ('" + [id] + "', '" + token + "')"
            connDB.query(inserttoken, (error, resultstoken, fields) => {
                console.log('Insert token')
            })
        } else {
            var updatetoken = "UPDATE token SET token = '" + token + "' WHERE idUser = ?"
            connDB.query(updatetoken, [id], (error, results, fields) => {
                console.log('Update token')
            })
        }
    })
}
const express = require('express') // เรียกใช้ Express
const connDB = require('./database/connDB.js')
const bodyParser = require('body-parser')
const port = 3333
const app = express()
const cors = require('cors')
const path = require('path')
const multer = require('multer')
const passwordHash = require('password-hash')

const CreateToken = require('./routes/controller/token')
const errorMessage = require('./routes/const/errorMessage')
const login = require('./routes/login')
const showfriends = require('./routes/friends')
const showDetailFriend = require('./routes/friends')
const addfriend = require('./routes/friends')
const unfriend = require('./routes/friends')
const searchfriend = require('./routes/friends')
const editprofile = require('./routes/editProfile')
const changpassword = require('./routes/login')

app.use(bodyParser.json())
app.use(cors())
app.use(express.static(path.join(__dirname, 'public')))

const storage = multer.diskStorage({
  destination: "./public/uploads",
  filename: function (req, file, cb) {
    cb(null, "IMAGE-" + Date.now() + path.extname(file.originalname));
  }
})

const upload = multer({ storage: storage })

//REGISTER
app.post('/register', upload.single('profileImage'), (req, res) => {
  let username = req.body.username
  let sqluser = "SELECT * FROM users WHERE username = ?"
  connDB.query(sqluser, [username], (error_checkUser, results, fields) => {
    if (error_checkUser) {
      console.log(error_checkUser)
      res.send(
        JSON.stringify(errorMessage.err_database)
      )
    }
    if (results[0] === undefined) {
      let email = req.body.email
      let sqlemail = "SELECT * FROM users WHERE email = ?"
      connDB.query(sqlemail, [email], (error_checkEmail, results_email, fields_email) => {
        if (error_checkEmail) {
          console.log(error_checkEmail)
          res.send(
            JSON.stringify(errorMessage.err_database)
          )
        }
        if (results_email[0] === undefined) {
          let citizen = req.body.citizen
          let sqlcitizen = "SELECT * FROM users WHERE citizen = ?"
          connDB.query(sqlcitizen, [citizen], (error_citizen, results, fields) => {
            if (error_citizen) {
              console.log(error_citizen)
              res.send(
                JSON.stringify(errorMessage.err_database)
              )
            }
            if (results[0] === undefined) {
              let firstname = req.body.firstname
              let lastname = req.body.lastname
              let username = req.body.username
              let password = passwordHash.generate(req.body.password)
              let email = req.body.email
              let mobile = req.body.mobile
              let citizen = req.body.citizen
              let profileImage = req.file.filename
              let sql = "INSERT INTO users (firstname, lastname, username, password, email, mobile, citizen, profileImage) VALUES ('" + firstname + "', '" + lastname + "', '" + username + "', '" + password + "', '" + email + "', '" + mobile + "', '" + citizen + "', '" + profileImage + "')"
              connDB.query(sql, (error_insertUsers, results, fields) => {
                if (error_insertUsers) {
                  console.log(error_insertUsers)
                  res.send(
                    JSON.stringify(errorMessage.err_database)
                  )
                }
                if (results[0] === undefined) {
                  let firstname_friend = req.body.firstname
                  let lastname_friend = req.body.lastname
                  let username_friend = req.body.username
                  let email_friend = req.body.email
                  let mobile_friend = req.body.mobile
                  let imageFriend = req.file.filename
                  let sql = "INSERT INTO friends (firstname_friend, lastname_friend, username_friend, email_friend, mobile_friend, imageFriend, status) VALUES ('" + firstname_friend + "', '" + lastname_friend + "', '" + username_friend + "', '" + email_friend + "', '" + mobile_friend + "', '" + imageFriend + "', 0)"
                  connDB.query(sql, (error_insertFriends, results, fields) => {
                    if (error_insertFriends) {
                      console.log(error_insertFriends)
                      res.send(
                        JSON.stringify(errorMessage.err_database)
                      )
                    } else {
                      res.send({
                        code: '0000',
                        message: 'Register success. Please login.',
                        results: JSON.stringify(results)
                      })
                    }
                  })
                }
              })
            } else {
              res.send(
                JSON.stringify(errorMessage.err_citizen_already)
              )
            }
          })
        } else {
          res.send(
            JSON.stringify(errorMessage.err_email_already)
          )
        }
      })
    } else {
      res.send(
        JSON.stringify(errorMessage.err_username_already)
      )
    }
  })
})

// LOGIN
app.post('/login', login)
app.post('/changpassword', changpassword)

//UPDATEPROFILE
app.put('/editprofile/:idUser', editprofile)

app.put('/editprofileimage/:idUser', upload.single('newprofileImage'), CreateToken.validateToken(), CreateToken.updateNewToken(), (req, res) => {
  let idUser = req.body.idUser
  let sqliduser = "SELECT * FROM users WHERE idUser = ?"
  connDB.query(sqliduser, [idUser], (error_checkidUser, results, fields) => {
    if (error_checkidUser) {
      console.log('error_checkUser', error_checkidUser)
      res.send(
        JSON.stringify(errorMessage.err_database)
      )
    } if (results[0] !== undefined) {
      let profileImage = req.file.filename
      let sqlupdateimageUser = "UPDATE users SET profileImage = '" + profileImage + "' WHERE idUser = ?"
      connDB.query(sqlupdateimageUser, [idUser], (error_updateUser, results, fields) => {
        if (error_updateUser) {
          console.log('error_updateUser', error_updateUser)
          res.send(
            JSON.stringify(errorMessage.err_database)
          )
        } else {
          let imageFriend = req.file.filename
          let sqlupdateimageUser = "UPDATE friends SET imageFriend = '" + imageFriend + "' WHERE idFriend = ?"
          connDB.query(sqlupdateimageUser, [idUser], (error_updateFriend, results, fields) => {
            if (error_updateFriend) {
              console.log('error_updateFriend', error_updateFriend)
              res.send(
                JSON.stringify(errorMessage.err_database)
              )
            } else {
              let sqluserDetail = "SELECT * FROM users WHERE idUser = ?"
              connDB.query(sqluserDetail, [idUser], (error_select_detailUser, results) => {
                if (error_select_detailUser) {
                  console.log('error_select_detailUser', error_select_detailUser)
                  res.send(
                    JSON.stringify(errorMessage.err_database)
                  )
                } else {
                  res.send({
                    code: '0000',
                    message: 'Success',
                    results: JSON.stringify(results),
                    token: req.token
                  })
                }
              })
            }
          })
        }
      })
    } else {
      res.send(
        JSON.stringify(errorMessage.err_username_already)
      )
    }
  })
})

// FRIENDS
app.get('/showfriend', showfriends) //เรียกดูเพื่อนที่เป็นเพื่อน
app.post('/search', searchfriend) //ค้นหาเพื่อน
app.get('/friend/:idFriend', showDetailFriend) //เรียกดูโปรไฟล์เพื่อน
app.put('/addfriend/:idFriend', addfriend) //เพิ่มเพื่อน
app.put('/unfriend/:idFriend', unfriend) //เลิกเป็นเพื่อน

// //Delete
// app.delete('/user/:idUser', (req, res) => {
//   let sql = "DELETE FROM users WHERE idUser=" + req.params.idUser + ""
//   connDB.query(sql, (err) => {
//     if (err) throw err
//     res.end(JSON.stringify('Record has been deleted!'))
//   })
// })


//Server listening
app.listen(port, () => {     // 
  console.log('start port', port)
})